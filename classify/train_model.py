import json
import os
import sys
import numpy as np
import pandas as pd
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.svm import SVC


def first_char(s: str) -> int:
    assert(s[0].isalpha())
    return (ord(s[0]) - ord('a'))


def read_data() -> pd.DataFrame:
    df = pd.DataFrame()

    for file in sorted(os.listdir('resources')):
        if file.endswith('.csv'):
            temp_df = pd.read_csv(f'resources/{file}', header=None)
            df = pd.concat((df, temp_df[9:]), ignore_index=True)

    df.columns = ['firstchar', 'namelen', 'matchlen', 'bothstart', 'class']
    df['firstchar'] = df['firstchar'].map(first_char)

    return df


data = read_data()
x = data.drop('class', axis=1)
y = data['class']

svclassifier = SVC(kernel='poly', degree=1, gamma=0.05)
svclassifier.fit(x, y)

y_pred = svclassifier.predict(x)
print(confusion_matrix(y, y_pred))
print(classification_report(y, y_pred))

filenames = sorted(os.listdir('resources'))
filenames = [file for file in filenames if file.endswith('.csv')]

print('report:')
for i in range(len(data)):
    if y[i] != y_pred[i]:
        row = x.iloc[i]
        print(
            f'{i:5d} ({filenames[i // (922-9)].rjust(22)}:{i % (922-9)+10:3d}) || ', end='')
        print(
            f'{chr(row["firstchar"]+ord("a"))} {row["namelen"]:2d} {row["matchlen"]} {row["bothstart"]} || ', end='')
        print(f'{y[i]} => {y_pred[i]}')

print(sum(svclassifier.n_support_), 'support vectors', svclassifier.n_support_,)

model = {}
model['degree'] = svclassifier.degree
model['intercept'] = svclassifier.intercept_[0]

# linear: we can combine the support vectors to speed up the decision function
vector = np.zeros(x.shape[1])
for i in range(sum(svclassifier.n_support_)):
    vector += svclassifier.gamma * svclassifier.dual_coef_[0][i] * svclassifier.support_vectors_[i]
model['vector'] = list(vector)

if type(svclassifier.gamma) not in (float, int):
    print('warning: gamma is not a number')

with open('svm_params.json', 'w') as fout:
    fout.write(json.dumps(model))
