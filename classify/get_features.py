import json
import re
import sys
import os
import numpy as np


class Features:
    MAX_LEN = 32

    query = ''
    n = 0
    matrix = np.zeros((MAX_LEN, MAX_LEN), dtype=np.int32)
    prev = ""
    prev_lcs = np.zeros(MAX_LEN, dtype=np.int32)
    prev_start = False

    @classmethod
    def init(cls, q: str):
        cls.query = q.strip().lstrip("@").lower()
        cls.n = len(cls.query)

    @classmethod
    def compute(cls, name: str) -> list:
        s = re.sub(r'[^a-z]+', '', name)
        m = len(s)

        if m == 0:
            return [name, m, 0, 0]

        same = 0
        for i in range(min(m, len(cls.prev))):
            if s[i] == cls.prev[i]:
                same = i + 1
            else:
                break

        lcs = cls.prev_lcs[same]
        start = same != 0 and cls.prev_start

        for i in range(same, m):
            for j in range(i, cls.n):
                if s[i] == cls.query[j]:
                    cand = cls.matrix[i + 1][j + 1] = cls.matrix[i][j] + 1
                    if i + 1 == cand and cand > lcs:
                        lcs = cand
                        if i == j:
                            start = True
                else:
                    cls.matrix[i + 1][j + 1] = 0
            cls.prev_lcs[i + 1] = lcs

        cls.prev = s
        cls.prev_start = start
        return [name, m, lcs, 1 if start else 0]


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(f'usage: {sys.argv[0]} QUERY')
        sys.exit(1)

    Features.init(sys.argv[1])

    filename = f'resources/{Features.query}.csv'
    if os.path.exists(filename):
        print(f"file {filename} exists")
        sys.exit(0)

    with open(f'resources/{Features.query}.csv', 'w') as fout:
        with open('resources/public-commands.json', 'r') as fin:
            commands = json.loads(fin.read())['commands']

        for command in commands:
            f = Features.compute(command['name'])
            f = [str(x) for x in f]
            fout.write(f'{",".join(f)},0\n')
